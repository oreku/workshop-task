<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'user_id' =>  1,
        'title' => $faker->sentence(2, true),
        'description' => $faker->sentence(12, true),
        'date' => $faker->date('Y-m-d', 'now'),
        'time' => $faker->time('H:i', 'now'),
        'complete' =>  $faker->boolean,
    ];
});
