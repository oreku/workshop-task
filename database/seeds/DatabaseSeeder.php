<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    
    public function run()
    {
        factory(App\User::class)->create();
        factory(App\Task::class, 32)->create();
    }
}
