<?php

return [
    'expiration' => env('TOKEN_EXPIRATION', '10800')
];