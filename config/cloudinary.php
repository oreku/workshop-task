<?php

return [
    'name' => env('CLOUDINARY_NAME'),
    'key' => env('CLOUDINARY_KEY'),
    'secret' => env('CLOUDINARY_SECRET'),
];