<?php

Route::post('signup', 'UserController@signup');
Route::post('signin', 'UserController@signin');

Route::group(['middleware' => ['auth.token']], function () {
    Route::apiResource('task', 'TaskController');
    Route::post('user/profilepicture', 'UploadController@uploadProfilePicture')->name('user.picture');
});