<?php

namespace App\Http\Controllers;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function signup(Request $request)
    {
        
        $rules = [
            'name' => 'required|max:64',
            'email' => 'required|email|max:255|unique:users',
            'profile_picture' => 'url',
            'password' => 'required|min:8',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'code' => 'rnv',
                'message' => 'La información del usuario no es valida',
                'errors' => $validator->errors(),
            ], 400);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile_picture = $request->profile_picture;
        $user->password = Hash::make($request->password);
        $user->save();

        $token = $this->makeToken($user);

        return response()->json([
            'success' => true,
            'code' => 'ok',
            'message' => 'Usuario registrado de forma exitosa',
            'data' => $user,
            'token' => $token,
        ], 201)->header('Authorization', "Bearer $token");

    }

    public function signin(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255|exists:users',
            'password' => 'required|min:8',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'La petición no es valida',
                'errors' => $validator->errors()
            ], 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!Hash::check($request->password, $user->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Estas credenciales no concuerdan con nuestros registros'
            ], 400);
        }

        $token = $this->makeToken($user);
        
        return response()->json([
            'success' => true,
            'message' => 'Token generado de forma exitosa',
            'data' => $user,
            'token' => $token
        ], 200)->header('Authorization', "Bearer $token");
    }

    public function makeToken($user)
    {
        $key = config('app.key');

        $token = [
            'sub' => $user->id,
            'name' => $user->name,
            'iat' => time(),
            'exp' => time() + config('jwt.expiration'),
        ];

        $jwt = JWT::encode($token, $key);

        return $jwt;
    }
}
