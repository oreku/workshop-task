<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Validator;

class TaskController extends Controller
{

    public function index()
    {
        $tasks = Task::paginate();
        return response()->json([
            'sucesss' => true,
            'code' => 'ok',
            'data' => $tasks
        ], 200);
    }

    public function store(Request $request)
    {

        $rules = [
            'user_id' => 'required|exists:users,id',
            'title' => 'required|max:64',
            'description' => 'max:255',
            'date' => 'date_format:Y-m-d',
            'time' => 'date_format:H:i',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'code' => 'rnv',
                'message' => 'La petición no es valida',
                'errors' => $validator->errors()
            ], 400);
        }

        $task = Task::create($request->all());

        return response()->json([
            'success' => true,
            'code' => 'ok',
            'message' => 'Tarea registrada de forma exitosa',
            'data' => $task
        ], 201);

    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return response()->json([
            'success' => true,
            'code' => 'ok',
            'message' => 'Tarea encontrada correctamente',
            'data' => $task
        ], 200);
    }

    public function update(Request $request, $id)
    {

        $task = Task::findOrFail($id);

        $rules = [
            'user_id' => 'required|exists:users,id',
            'title' => 'required|max:64',
            'description' => 'max:255',
            'date' => 'date_format:Y-m-d',
            'time' => 'date_format:H:i',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'code' => 'rnv',
                'message' => 'La petición no es valida',
                'errors' => $validator->errors()
            ], 400);
        }

        $task->user_id = $task->user_id;
        $task->title = $task->title;
        $task->description = $task->description;
        $task->date = $task->date;
        $task->time = $task->time;
        $task->save();

        return response()->json([
            'success' => true,
            'code' => 'ok',
            'message' => 'Tarea actualizada de forma exitosa',
            'data' => $task
        ], 201);
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return response()->json([
            'success' => true,
            'code' => 'ok',
            'message' => 'Tarea eliminada de forma exitosa',
            'data' => $task
        ], 201);
    }
}
