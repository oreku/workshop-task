<?php

namespace App\Http\Controllers;

use App\User;
use Cloudinary;
use Illuminate\Http\Request;
use Validator;

class UploadController extends Controller
{
    public function uploadProfilePicture(Request $request)
    {

        $rules = [
            'profile_picture' => 'required|max:10000',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'code' => 'x202',
                'message' => 'La peticion no es valida',
                'errors' => $validator->errors(),
            ], 400);
        }

        Cloudinary::config([
            'cloud_name' => config('cloudinary.name'),
            'api_key' => config('cloudinary.key'),
            'api_secret' => config('cloudinary.secret'),
        ]);

        $id = $request->user_id;
        $user = User::findOrFail($id);

        if ($user->profile_picture !== null) {
            $public_id = $user->cloudinary_public_id;
            try {
                Cloudinary\Uploader::destroy($public_id, $options = array());
            } catch (\Exeption $e) {
                return response()->json([
                    'success' => false,
                    'message' => 'La imagen existente del usuario no se pudo elimnar',
                ], 500);
            }
        }

        try {
            $result = Cloudinary\Uploader::upload($request->profile_picture, [
                'folder' => 'avatars',
            ]);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Hubo un error al subir la imagen',
            ], 500);
        }

        $user->profile_picture = $result['url'];
        $user->cloudinary_public_id = $result['public_id'];
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Imagen cargada correctamente',
            'data' => [
                'url' => $result['url'],
            ],
        ], 200);
    }
}
