<?php

namespace App\Http\Middleware;
use Firebase\JWT\JWT;

use Closure;

class CheckToken
{

    public function handle($request, Closure $next)
    {

        $token = $request->bearerToken();
        $key = config('app.key');

        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'El token ya no es valido',
            ], 401);
        }

        $request->merge(['user_id' => $decoded->sub]);

        return $next($request);
    }
}
